var container;
var camera, scene, renderer, group, particle;
var mouseX = 0, mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
init();
animate();
function init() {
	container = document.getElementById('container');
	document.body.appendChild( container );
	camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 9000 );
	camera.position.z = 2200;
	scene = new THREE.Scene();
	scene.background = new THREE.Color( 0xffffff );
	var PI2 = Math.PI * 2;
	var program = function ( context ) {
		context.beginPath();
		context.arc( 0, 0, 0.3, 0, PI2, true );
		context.fill();
	};
	group = new THREE.Group();
	scene.add( group );
	var geometry = new THREE.Geometry();

	for ( var i = 0; i < 500; i++ ) {
		var material = new THREE.SpriteCanvasMaterial( {
			color: 0x000	, 
			program: program
		} );
		particle = new THREE.Sprite( material );
		particle.position.x = Math.random() * 2000 - 1000;
		particle.position.y = Math.random() * 2000 - 1000;
		particle.position.z = Math.random() * 2000 - 1000;
		particle.scale.x = particle.scale.y = Math.random() * 20 + 10;

		geometry.vertices.push(
			new THREE.Vector3( particle.position.x, particle.position.y, particle.position.z )
			);

		group.add( particle );
		
	}

	var geometry2 = new THREE.BoxGeometry( 2000, 2000, 2000 );
	var material = new THREE.MeshBasicMaterial( { 
		color : 0x000000,
		opacity : 0.1, 
		wireframe: true } );
	var cube = new THREE.Mesh( geometry2, material );
	group.add( cube );

	var line = new THREE.Line( geometry, new THREE.LineBasicMaterial({
		color : 0x000000,
		opacity : 0.07
	}));
	group.add(line);

	renderer = new THREE.CanvasRenderer();
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	container.appendChild( renderer.domElement );

	document.addEventListener( 'mousemove', onDocumentMouseMove, false );
	document.addEventListener( 'touchstart', onDocumentTouchStart, false );
	document.addEventListener( 'touchmove', onDocumentTouchMove, false );
				//
				window.addEventListener( 'resize', onWindowResize, false );
			}
			function onWindowResize() {
				windowHalfX = window.innerWidth / 2;
				windowHalfY = window.innerHeight / 2;
				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();
				renderer.setSize( window.innerWidth, window.innerHeight );
			}
			//
			function onDocumentMouseMove( event ) {
				mouseX = event.clientX - windowHalfX;
				mouseY = event.clientY - windowHalfY;
				camera.lookAt(scene.position);
			}
			function onDocumentTouchStart( event ) {
				if ( event.touches.length === 1 ) {
					event.preventDefault();
					mouseX = event.touches[ 0 ].pageX - windowHalfX;
					mouseY = event.touches[ 0 ].pageY - windowHalfY;
				}
			}
			function onDocumentTouchMove( event ) {
				if ( event.touches.length === 1 ) {
					event.preventDefault();
					mouseX = event.touches[ 0 ].pageX - windowHalfX;
					mouseY = event.touches[ 0 ].pageY - windowHalfY;
				}
			}
			//
			function animate() {
				requestAnimationFrame( animate );
				render();

			}
			function render() {
				camera.position.x += ( mouseX - camera.position.x ) * 0.5;
				camera.position.y += ( - mouseY - camera.position.y ) * 0.5;
				camera.lookAt( scene.position );
				group.rotation.x += 0.002;
				group.rotation.y += 0.002;
				renderer.render( scene, camera );
			}



/*var container = document.getElementById('container');

var renderer = new THREE.CanvasRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
container.appendChild( renderer.domElement );

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 10000 );
scene.add(camera);



var particle = new THREE.Particle( material ); 

var material = new THREE.ParticleCanvasMaterial({
	color : 0x3c7587,
	opacity: 1,
	parogram: function (context) {
		context.beinPath();
		context.arc(0,0,1,0,Math.PI * 2, true);
		context.closePath();
		context.fill();
	}
});

particle.position.x = 0;
particle.position.y = 0;
particle.position.z = 0;

scene.add(particle);
camera.position.z = 100;
camera.lookAt(particule);
renderer.render( scene, camera );*/

/*var geometry = new THREE.TorusKnotGeometry( 10, 3, 100, 16 );
var material = new THREE.MeshBasicMaterial( { color: 0x3c7587 } );
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );

camera.position.z = 50;*/

/*var material = new THREE.LineBasicMaterial( { color: 0x3c7587} );

var geometry = new THREE.Geometry();
geometry.vertices.push(new THREE.Vector3(-10, 0, 0));
geometry.vertices.push(new THREE.Vector3(0, 10, 0));
geometry.vertices.push(new THREE.Vector3(10, 0, 0));
geometry.vertices.push(new THREE.Vector3(0, 0, 10));
geometry.vertices.push(new THREE.Vector3(-10, 0, 0));


var line = new THREE.Line(geometry, material);
scene.add( line );
renderer.render( scene, camera );*/

/*function animate() {
	requestAnimationFrame( animate );

	cube.rotation.x += 0.04;
	cube.rotation.y += 0.01;

	renderer.render( scene, camera );
}
animate();*/