export class Book {
    id: number;
    title: string;
    content: string;
    link: string;
    image: string;
}