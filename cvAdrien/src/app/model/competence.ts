export class Competence {
    id: number;
    nom: string;
    niveau: number;
    type: string;
}