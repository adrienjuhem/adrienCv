export class Experience {
    id: number;
    poste: string;
    date: string;
    entreprise: string;
    lieu: string;
    url: string;
    photo: string;
    resume: string;
    text: string;
    type: string;
    title_short: string;
}