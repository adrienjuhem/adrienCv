export class Message {
    from: string;
    to: string;
    subject: string;
    text: string;
}