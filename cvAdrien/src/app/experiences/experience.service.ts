import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../environment';
import { Experience } from '../model/experience';

@Injectable()
export class ExperienceService {

  constructor(private http: Http) { }

  private url = environment.url;

 getExperiences(): Observable<Experience[]> {
        let expUrl = this.url + "experiences"; 
        return this.http.get(expUrl)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().message || 'Server error'));

    }
   
     getExperience(id: number): Observable<Experience> {
        let expUrl = this.url + "experiences"; 
        return this.http.get(expUrl + '/' + id, )
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().message || 'Server error'));

    }
 
}
