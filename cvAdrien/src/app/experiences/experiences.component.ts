import { Component, OnInit, HostListener } from '@angular/core';
import { Experience } from '../model/experience';
import { ExperienceService } from './experience.service';


@Component({
	selector: 'app-experiences',
	templateUrl: './experiences.component.html',
	styleUrls: ['./experiences.component.css']
})
export class ExperiencesComponent implements OnInit {

	
	constructor( private experienceService: ExperienceService ) { }

	private experiences: Experience[];
	private exp: Experience;
	

	private getExperiences(){
		this.experienceService.getExperiences().subscribe(
			experiences => {
			  this.experiences = experiences;
			},
			err => {
			  console.log(err);
			});
	}
	
	ngOnInit() {
		this.getExperiences();
	}

}
