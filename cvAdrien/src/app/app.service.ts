import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod }  from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {URLSearchParams, QueryEncoder} from '@angular/http';


import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Message } from './model/message';
import { environment } from './environment';

declare var emailjs : any;

@Injectable()
export class AppService {

  private url = 'https://api:key-50c5b627fbc07704e80b20f122b5b981@api:key-50c5b627fbc07704e80b20f122b5b981@api.mailgun.net/v3/sandboxf346a32047f140d6b405db1fda0e127b.mailgun.org/messages';
  constructor(private http: Http) { }

  postEmail(email: string, name: string, text:string ): Observable<any[]>{

    return emailjs.send("gmail","contact_form",{name: name, email: email, message : text})
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }

  

}

