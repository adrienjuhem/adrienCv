import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { HttpModule }           from '@angular/http'
import { NgCircleProgressModule } from 'ng-circle-progress';

import { AppComponent } from './app.component';
import { IntroComponent } from './intro/intro.component';
import { NavbarComponent } from './navbar/navbar.component';
import { WebComponent } from './web/web.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { ExperiencesComponent } from './experiences/experiences.component';
import { CompetencesComponent } from './competences/competences.component';
import { ExperienceComponent } from './experience/experience.component';
import { BookComponent } from './book/book.component';

import { AppService } from './app.service';
import { ExperienceService } from './experiences/experience.service';
import { CompetencesService } from './competences/competences.service';
import { BookService } from './book/book.service';

const appRoutes: Routes = [
  { path: 'intro', component: IntroComponent },
  { path: '', redirectTo: '/intro', pathMatch: 'full'},
  { path: '', component: NavbarComponent, children: [
      { path: 'home', component: HomeComponent },
      { path: 'competence', component: CompetencesComponent },
      { path: 'exp', component: ExperiencesComponent },
      { path: 'web', component: WebComponent },
      { path: 'book', component: BookComponent },
      { path: 'contact', component: ContactComponent },
  ] },  
  { path: '**', component: IntroComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    IntroComponent,
    NavbarComponent,
    WebComponent,
    ContactComponent,
    HomeComponent,
    ExperiencesComponent,
    CompetencesComponent,
    ExperienceComponent,
    BookComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    HttpClientModule,
    NgCircleProgressModule.forRoot({
      radius: 60,
      space: -10,
      outerStrokeWidth: 10,
      outerStrokeColor: "#60AB64",
      innerStrokeColor: "#e7e8ea",
      innerStrokeWidth: 10,
      title: "php",
      animateTitle: false,
      animationDuration: 1000,
      showUnits: false,
      showBackground: false,
      clockwise: false
    })
  ],
  providers: [
  AppService, 
  ExperienceService,
  CompetencesService,
  BookService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
