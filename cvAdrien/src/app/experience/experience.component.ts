import { Component, OnInit, Input } from '@angular/core';
import { Experience } from '../model/experience';

@Component({
  selector: '[app-experience]',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css']
})
export class ExperienceComponent implements OnInit {

@Input() exp: Experience;
private modalShow : boolean = true;
  constructor() { }

  ngOnInit() {
  }

}
