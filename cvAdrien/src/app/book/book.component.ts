import { Component, OnInit } from '@angular/core';
import { BookService } from './book.service';
import { Book } from '../model/book';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  constructor(
    private bookService: BookService
  ) { }

  private books: Book[];

  private getCompetences(){
    this.bookService.getBooks().subscribe(
      books => {
        this.books = books;
      }, error => {
        console.log(error);
      });
    }
  ngOnInit() {
    this.getCompetences();
  }

}
