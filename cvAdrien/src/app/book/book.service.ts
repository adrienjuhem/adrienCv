import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../environment';
import { Book } from '../model/book';

@Injectable()
export class BookService {

  constructor(private http: Http) { }


  private url = environment.url;

    getBooks(): Observable<Book[]> {
      let expUrl = this.url + "books"; 
      return this.http.get(expUrl)
          .map((res: Response) => res.json())
          .catch((error: any) => Observable.throw(error.json().message || 'Server error'));

    }

}
