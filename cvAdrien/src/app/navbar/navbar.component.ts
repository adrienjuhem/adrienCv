import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

	constructor() { }
	
	private showMenu: boolean;

 	public loadScript(url) {
		let node = document.createElement('script');
		node.src = url;
		node.type = 'text/javascript';
		document.getElementsByTagName('head')[0].appendChild(node);
	}

	ngOnInit() {
	 	this.loadScript('assets/js/classie.js');
	 	this.loadScript('assets/js/clipboard.min.js');
	 	this.loadScript('assets/js/modernizr.custom.js');
	 	this.loadScript('assets/js/menu.js');
  }

}
