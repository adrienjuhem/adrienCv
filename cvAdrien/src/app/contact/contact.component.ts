import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Message } from '../model/message';
import { environment } from '../environment';
import {Renderer} from '@angular/core';

declare var emailjs : any;

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  providers: [AppService]
})
export class ContactComponent implements OnInit {

  constructor(
    private appService: AppService,
    private render:Renderer,
    ) { }


  email: string;
  name: string;
  text: string;
  info: string;
  color: string;
  alert: boolean = false;
  loading: boolean = false;
  emailVerif: boolean;
  nameVerif: boolean;
  textVerif: boolean;
  regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  postEmail() {

    this.info = "";
    this.alert = false;
    
    emailjs.send(environment.mail, environment.template,{name: this.name, email: this.email, message : this.text})
    .then(function(response) {    
      console.log("SUCCESS. status=%d, text=%s", response.status, response.text);
      this.email = "";
      this.name = "";
      this.text = "";
      this.alert = true;
      this.info = "message envoyé";
      this.color = "alert alert-success";
      this.loading = false;
      if (this.getCookie("emailCount") == null) {
         this.setCookie("emailCount", 1);

      } else if (parseInt(this.getCookie("emailCount")) < 3) {
        let newValue = parseInt(this.getCookie("emailCount")) + 1;
        this.setCookie("emailCount", newValue);
      }  

    }.bind(this), function(err) {

      console.log("FAILED. error=", err);
      this.alert = true;
      this.info = "une erreur c'est produite veuillez recommencé";
      this.color = "alert alert-danger";
      this.loading = false;
    }.bind(this));
  }

  ngOnInit() {

  }

  setCookie(name, value) {
    var today = new Date(), expires = new Date();
    expires.setTime(today.getTime() + (24 * 60 * 60 * 1000));
    document.cookie = name + "=" + encodeURIComponent(value) + ";expires=" + expires;
  }

  getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return null;
  }

  emailSendedCheck(): boolean {
    if (this.getCookie("emailCount") == null) {
      return false;
    } else if (parseInt(this.getCookie("emailCount")) < 3) {
      return false;
    } else {
      return true;
    }

  }

  sendEmail() {

    this.emailVerif = false;
    this.nameVerif = false;
    this.textVerif = false;

    if (this.emailSendedCheck()) {
      this.alert = true;
      this.color = "alert alert-danger";
      return this.info="Vous devez attendre 24h pour enoyer un nouveau message";
    }
    else if(this.name == undefined || this.name == "") {
      this.alert = true;
      this.color = "alert alert-danger";
      this.nameVerif = true;
      return this.info=" Le champ nom est obligatoire";
      

    } else if (this.email == undefined || this.email == "") {
      this.alert = true;
      this.color = "alert alert-danger";
      this.emailVerif = true;
      return this.info=" Le champ mail est obligatoire";

    } else if (!this.regEmail.test(this.email)) {
      this.emailVerif = true;
      this.alert = true;
      this.color = "alert alert-danger";
      return this.info="Cet email n'est pas conforme";

    } else if (this.text == undefined || this.text == "") {
      this.textVerif = true;
      this.alert = true;
      this.color = "alert alert-danger";
      return this.info=" Le champ text est obligatoire";

    } else {
      console.log(this.getCookie("emailCount"));
      //this.setCookie("emailCount", 1);

      console.log("send");
      this.loading = true;
      this.postEmail();
    }

  }

}
