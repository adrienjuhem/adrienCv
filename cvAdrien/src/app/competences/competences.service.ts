import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../environment';
import { Competence } from '../model/competence';

@Injectable()
export class CompetencesService {

  constructor(private http: Http) { }
  
  private url = environment.url;

    getCompetences(): Observable<Competence[]> {
      let expUrl = this.url + "competences"; 
      return this.http.get(expUrl)
          .map((res: Response) => res.json())
          .catch((error: any) => Observable.throw(error.json().message || 'Server error'));

    }

    getCompetence(id: number): Observable<Competence> {
        let expUrl = this.url + "competences"; 
        return this.http.get(expUrl + '/' + id, )
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().message || 'Server error'));

    }

}
