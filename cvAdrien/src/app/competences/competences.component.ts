import { Component, OnInit } from '@angular/core';
import { CompetencesService } from './competences.service';
import { Competence } from '../model/competence';

@Component({
  selector: 'app-competences',
  templateUrl: './competences.component.html',
  styleUrls: ['./competences.component.css'],
  providers: [],
})
export class CompetencesComponent implements OnInit {

  constructor(private competencesService: CompetencesService) { }
  private competences: Competence[];
  private type : string;

  private getCompetences(){
    this.competencesService.getCompetences().subscribe(
      competences => {
        this.competences = competences;
      }, error => {
        console.log(error);
      });
  }
  
  private filter(choice){
    this.type = choice;
  }
  ngOnInit() {
    this.type = "web";
    this.getCompetences();
  }

}
