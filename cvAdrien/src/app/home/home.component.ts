import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }
  private age: number;
  ngOnInit() {
    let date = new Date();
    let birth =new Date(1988, 0, 14);
    this.age = date.getFullYear() - birth.getFullYear();
    console.log("////// Made with ♥ in Angular 4 and Symfony 3 //////");
    console.log("");
    console.log("///////////////////////////////////////////////////////");
    console.log("Please contact me at : ");
    console.log( "adrien.juhem@gmail.com");
    console.log( "or");
    console.log( "+33 6 38 67 46 31");
    console.log("///////////////////////////////////////////////////////");
    console.log("");

    console.log("////// Enjoy your visit //////");
  }

}
