import { CvAdrienPage } from './app.po';

describe('cv-adrien App', () => {
  let page: CvAdrienPage;

  beforeEach(() => {
    page = new CvAdrienPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
