<?php

namespace CvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use CvBundle\Entity\Experience;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ExperienceController extends Controller
{
	/**
     * @Rest\Get("experiences/")
     * @view()
     */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();

		$experiences = $em->getRepository('CvBundle:Experience')->getAllByDate();


		return $experiences;
	}


	/**
     * @Rest\Get("experiences/{id}")
     * @view()
     */
	public function showAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$experience = $em->getRepository('CvBundle:Experience')->findById($id);

		if (empty($experience)) {
			return new JsonResponse(['message' => 'experience not found'], Response::HTTP_NOT_FOUND);
		}

		return $experience;
	}
}
