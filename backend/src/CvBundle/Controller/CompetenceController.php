<?php

namespace CvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use CvBundle\Entity\Competence;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CompetenceController extends Controller
{
	/**
     * @Rest\Get("competences/")
     * @view()
     */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();
		$competences = $em->getRepository('CvBundle:Competence')->findAll();

		return $competences;
	}


	/**
     * @Rest\Get("competences/{id}")
     * @view()
     */
	public function showAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$competence = $em->getRepository('CvBundle:Competence')->findById($id);

		if (empty($competence)) {
			return new JsonResponse(['message' => 'Competence not found'], Response::HTTP_NOT_FOUND);
		}

		return $competence;
	}
}
