<?php

namespace CvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use CvBundle\Entity\Book;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class BookController extends Controller
{
	/**
     * @Rest\Get("books/")
     * @view()
     */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();
		$books = $em->getRepository('CvBundle:Book')->findAll();

		return $books;
	}


	/**
     * @Rest\Get("books/{id}")
     * @view()
     */
	public function showAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$book = $em->getRepository('CvBundle:Book')->findById($id);

		if (empty($book)) {
			return new JsonResponse(['message' => 'book not found'], Response::HTTP_NOT_FOUND);
		}

		return $book;
	}
}
